function Square(props) {
    return (
      <button className={props.className} onClick={props.placeMove}>
        {props.value}
      </button>
    );
  }

export default Square;