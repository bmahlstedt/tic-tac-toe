import React from 'react';
import { calculateWinner, getColRow } from './utils';
import Board from './board';

class Game extends React.Component {
    boardSize = 3;
  
    constructor(props) {
      super(props);
      this.state = {
        history: [
          Array(9).fill(null)
        ],
        moveNumber: 0,
        xIsNext: true,
        moves: ['(0, 0)'],
        moveListAscending: true,
      };
      this.toggleMoveOrder = this.toggleMoveOrder.bind(this);
    }
  
    placeMove(i) {
      const history = this.state.history.slice(0, this.state.moveNumber + 1)
      const moves = this.state.moves.slice(0, this.state.moveNumber + 1)
      const current = history[history.length - 1]
      const squares = current.slice();
      if (squares[i] || calculateWinner(squares).winner) {
        return;
      }
      squares[i] = this.state.xIsNext ? 'X' : 'O';
      const move = getColRow(i, this.boardSize);
      this.setState(prevState => ({
        history: history.concat([squares]),
        moveNumber: history.length,
        xIsNext: !prevState.xIsNext,
        moves: moves.concat([move]),
      }));
    }
  
    jumpTo(moveNumber) {
      this.setState({
        moveNumber: moveNumber,
        xIsNext: (moveNumber % 2) === 0,
      });
    }
  
    toggleMoveOrder() {
      this.setState(prevState => ({
        moveListAscending: !prevState.moveListAscending
      }));
    }
  
    render() {
      const history = this.state.history;
      const current = history[this.state.moveNumber];
      const {winner, winningSquares} = calculateWinner(current);
      const moves = this.state.moves;
  
      const moveList = moves.map((move, moveNumber) => {
        let description = 'Go to move #' + moveNumber;
        return (
          <li key={moveNumber} style={{ 'fontWeight': moveNumber === this.state.moveNumber ? 'bold' : 'normal' }}>
            <button onClick={() => this.jumpTo(moveNumber)}>{description}</button>
            <span className="move">{move}</span>
          </li>
        );
      });
      if (!this.state.moveListAscending) {
        moveList.reverse();
      };
  
      let status;
      if (winner) {
        status = winner + ' won!'
      } else if (this.state.moveNumber === this.boardSize**2) {
        status = 'Board is full; game is a draw'
      } else {
        status = (this.state.xIsNext ? 'X' : 'O') + '\'s turn'
      }
  
      return (
        <div className="game">
          <div className="game-board">
            <Board
              boardSize={this.boardSize}
              squares={current}
              placeMove={(i) => this.placeMove(i)}
              winningSquares={winningSquares}
            />
          </div>
          <div className="game-info">
            <div>{status}</div>
            <button onClick={this.toggleMoveOrder}>Toggle move order</button>
            <ul>{moveList}</ul>
          </div>
      </div>
      );
    }
  }

export default Game;