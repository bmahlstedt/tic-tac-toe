import Square from './square';

function Board(props) {
    function renderSquare(props, i) {
      return (
        <Square
          key={i}
          value={props.squares[i]}
          placeMove={() => props.placeMove(i)}
          className={props.winningSquares.includes(i) ? "square green" : "square"}
        />
      );
    }
  
    let counter = 0;
    const rowSquares = [...Array(props.boardSize).keys()];
    const board = rowSquares.map(dummyRow => {
      const cols = rowSquares.map(dummyCol => {
        counter++;
        return (
          renderSquare(props, counter-1)
        );
      });
      return (
        <div key={dummyRow*100} className="board-row">{cols}</div>
      );
    });
  
    return (
      <div>{board}</div>
    );
  }

export default Board;